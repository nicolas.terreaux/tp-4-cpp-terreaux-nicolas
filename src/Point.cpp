//
// Created by Asraniel on 12.06.2021.
//



#include <functional>
#include <unordered_set>
#include <ostream>
#include <random>
#include <iostream>
#include "Point.h"


Point::Point(const Point &point) : x_(point.x_), y_(point.y_), name_(point.name_) {

}


Point::Point(double x, double y, std::string name) : x_(x), y_(y), name_(name) {

}


std::string Point::getName() const {
    return name_;
}

void Point::setX(double x) {
    this->x_ = x;
}

void Point::setY(double y) {
    this->y_ = y;
}


double Point::distance(const Point &point) const {
    return sqrt(pow(point.x_ - x_, 2) + pow(point.y_ - y_, 2));
}

Point &Point::operator+=(const Point &other) {
    x_ += other.x_;
    y_ += other.y_;
    return *this;
}

Point &Point::operator-=(const Point &other) {
    x_ -= other.x_;
    y_ -= other.y_;
    return *this;
}

Point &Point::operator*=(double other) {
    x_ *= other;
    y_ *= other;
    return *this;
}

Point &Point::operator/=(double other) {
    if (other == 0) {
        throw std::invalid_argument("Division by zero");
    }
    x_ /= other;
    y_ /= other;

    return *this;
}

Point Point::operator+(const Point &other) const {
    return Point(x_ + other.x_, y_ + other.y_);
}

Point Point::operator-(const Point &other) const{
    return Point(x_ - other.x_, y_ - other.y_);
}

Point Point::operator*(const double other) const{
    return Point(x_ * other, y_ * other);
}

Point Point::operator/(double other) {

    if (other == 0) {
        throw std::invalid_argument("Division by zero");
    }
    return Point(x_ / other, y_ / other);

    return Point(0, 0);
}

double Point::getX() const {
    return x_;
}

double Point::getY() const {
    return y_;
}


