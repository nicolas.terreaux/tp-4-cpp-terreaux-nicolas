//
// Created by Beat Wolf on 30.11.2021.
//

#ifndef CPPALGO_INTERVAL_H
#define CPPALGO_INTERVAL_H


template<typename T1 = int, typename T2 = double>
class Interval {
public:
    Interval(T1 start, T1 end, T2 value = 0) : start_(start), end_(end), value_(value) {};

    T1 getStart() const {
        return start_;
    }

    T1 getEnd() const {
        return end_;
    }

    T2 getValue() const {
        return value_;
    }

    bool contains(const T2 value) const {
        return value >= start_ && value <= end_;
    }


    // DOUBLE
    template<typename T>
    bool operator==(const Interval<T, double> &other) const {
        return start_ == other.start_ && end_ == other.end_ &&
               abs(value_ - other.value_) < std::numeric_limits<double>::epsilon();
    }

    template<typename T>
    bool operator==(const Interval<double, T> &other) const {
        return start_ == other.getStart() && end_ == other.getEnd() &&
               std::abs(value_ - other.getValue()) < std::numeric_limits<double>::epsilon();
    }


    bool operator==(const Interval<double, double> &other) const {
        return abs(start_ - other.getStart()) < std::numeric_limits<double>::epsilon() &&
               abs(end_ - other.getEnd()) < std::numeric_limits<double>::epsilon() &&
               abs(value_ - other.getValue()) < std::numeric_limits<double>::epsilon();
    }

    //FLOAT
    template<typename T>
    bool operator==(const Interval<T, float> &other) const {
        return start_ == other.start_ && end_ == other.end_ &&
               abs(value_ - other.value_) < std::numeric_limits<float>::epsilon();
    }

    template<typename T>
    bool operator==(const Interval<float, T> &other) const {
        return start_ == other.getStart() && end_ == other.getEnd() &&
               std::abs(value_ - other.getValue()) < std::numeric_limits<float>::epsilon();
    }

    bool operator==(const Interval<float, float> &other) const {
        return std::abs(start_ - other.getStart()) < std::numeric_limits<float>::epsilon() &&
               std::abs(end_ - other.getEnd()) < std::numeric_limits<float>::epsilon() &&
               std::abs(value_ - other.getValue()) < std::numeric_limits<float>::epsilon();
    }

    template<typename T11, typename T12>
    bool operator==(const Interval<T11, T12> &other) const {
        return std::abs(start_ - other.getStart()) < std::numeric_limits<T11>::epsilon() &&
               std::abs(end_ - other.getEnd()) < std::numeric_limits<T11>::epsilon() &&
               std::abs(value_ - other.getValue()) < std::numeric_limits<T12>::epsilon();
    }

    bool operator!=(const Interval &other) const {
        return *this != other;
    }

private:
    T1 start_;
    T1 end_;
    T2 value_;

};


#endif //CPPALGO_INTERVAL_H
