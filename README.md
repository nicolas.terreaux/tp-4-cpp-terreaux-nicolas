# TP4

## Exigences

Chaque exigence pas respectée enlève 0.1 de la note du TP.

- [X] Les tests unitaires passent sans erreurs
    - Avec les tests de Point du TP3
- [X] La spécialisation de template est utilisée
    - Suggestion: Interval ::operator== pour double (et float?)
- [X] L’exécutable benchmark (benchmark.cpp) existe et n’a pas été modifié
- [X] Aucun fichier dans src/base n’est modifié (sauf pour que le programme compile)
- [ ] Le code-review de votre partenaire a été fait
    - Voir feuille sur cyberlearn, personne A fait le review de personne B
- [X] Votre repository ne doit pas contenir des fichiers binaires et temporaires (comme le répertoire cmake-build-debug
  ou .idea)
- [X] Après chaque commit, le code compile sur le CI de gitlab avec -Wall et -Werror comme options et les tests
  unitaires sont exécutés
- [X] Vous travaillez sur un fork du repo original (lien sur cyberlearn) et devez créer un merge request avant la
  deadline.
    - Beat Wolf doit avoir accès à votre repository (ajouter comme membre ou mettre en public)
    - La personne qui doit faire votre code review doit aussi avoir accès

# TP5

## Exigences

- [X] Écrivez dans votre Readme.md du repository les résultats des benchmarks avant et après vos changements et l’output
  de valgrind avant et après corrections de problèmes
    - Si l’output est trop grand, mettez-le dans des fichiers annexes
- [X] Les fichiers PDFs qui montrent le profilage avant les optimisations et après
- [X] Le nom de l’exécutable du benchmark doit être « benchmark » (comme dans le CMakeList original)
    - Le code de benchmarch.cpp n’est pas modifié
- [X] Aucun fichier dans src/base est modifié
- [X] Votre repository ne doit pas contenir des fichiers binaires et temporaires (comme le répertoire cmake-build-debug
  ou .idea)
- [X] Après chaque commit, le code compile sur le CI de gitlab avec -Wall et -Werror comme options et les tests
  unitaires sont exécutés
- [X] Vous travaillez sur un fork du repo original (lien sur cyberlearn) et devez créer un merge request avant la
  deadline.
  -Beat Wolf doit avoir accès à votre repository (ajouter comme membre ou mettre en public)

## Valgrind

Avant de faire les corrections de problèmes:

![valgrind avant](/docs/valgrind_before.png)

Il a suffit de faire des deletes dans les méthodes qui appelaient leur méthode parente (createRandomPoints.)

Après les corrections de problèmes:

![valgrind après](/docs/valgrind_after.png)

## google-pprof

Profiling avant optimisation:
[profiling avant optimisation](/docs/profiling_without_optimisation.pdf)

Profiling après optimisation:
[profiling après optimisation](/docs/profiling_with_optimisation.pdf)

## Optimisation
```
----- Without optimisation -----
Finished calculation (1000) in avg. 55ms, total 279ms
Finished calculation (30000) in avg. 22873ms, total 114366ms
Finished calculation (30000) in avg. 27240ms, total 136204ms
Finished calculation (30000) in avg. 49743ms, total 248719ms

----- CHANGE DEBUG TO RELEASE -----
Finished calculation (1000) in avg. 12ms, total 62ms
Finished calculation (30000) in avg. 9626ms, total 48133ms
Finished calculation (30000) in avg. 11653ms, total 58268ms
Finished calculation (30000) in avg. 15112ms, total 75564ms

----- ADD PRAGMA IN SEARCH -----
Finished calculation (1000) in avg. 4ms, total 22ms
Finished calculation (30000) in avg. 1664ms, total 8323ms
Finished calculation (30000) in avg. 1813ms, total 9065ms
Finished calculation (30000) in avg. 2366ms, total 11834ms

----- ADD PRAGMA EVERYWHERE -----
Finished calculation (1000) in avg. 2ms, total 12ms
Finished calculation (30000) in avg. 1382ms, total 6914ms
Finished calculation (30000) in avg. 1500ms, total 7504ms
Finished calculation (30000) in avg. 2098ms, total 10493ms

----- ADD SOME MORE PRAGMA (MAYBE USELESS) -----
Finished calculation (1000) in avg.     4ms, total      23ms
Finished calculation (30000) in avg.    1499ms, total   7497ms
Finished calculation (30000) in avg.    1682ms, total   8412ms
Finished calculation (30000) in avg.    2335ms, total   11675ms

```